<?php
/**
 * @file
 * Contains price argument handler.
 */

/**
 * Basic argument handler for arguments that are numeric. Incorporates
 *
 * @ingroup views_argument_handlers
 */
class uc_price_filter_handler_argument_gele extends views_handler_argument {
  function option_definition() {
    $options = parent::option_definition();

    $options['ge'] = array('default' => FALSE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);


    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Select operator'),
      '#options' => array('ge' => t('>='), 'le' => t('<=')),
      '#default_value' => !empty($this->options['type'])? $this->options['type'] : 'ge',
    );
  }

  function title() {
    if (!$this->argument) {
      return !empty($this->definition['empty field name']) ? $this->definition['empty field name'] : t('Uncategorized');
    }


    if (empty($this->value)) {
      return !empty($this->definition['empty field name']) ? $this->definition['empty field name'] : t('Uncategorized');
    }

    if ($this->value === array(-1)) {
      return !empty($this->definition['invalid input']) ? $this->definition['invalid input'] : t('Invalid input');
    }

    return implode($this->title_query());
  }

  /**
   * Override for specific title lookups.
   */
  function title_query() {
    return $this->value;
  }

  function query() {
    $this->ensure_my_table();

    if (count($this->value) > 1) {
      $operator = empty($this->options['type']) || $this->options['type'] == 'ge' ? '>=' : '<=';
      $placeholders = implode(', ', array_fill(0, sizeof($this->value), '%d'));
      $this->query->add_where(0, "$this->table_alias.$this->real_field $operator ($placeholders)", $this->value);
    }
    else {
      $operator = empty($this->options['type'])  || $this->options['type'] == 'ge' ? '>=' : '<=';
      $this->query->add_where(0, "$this->table_alias.$this->real_field $operator %d", $this->argument);
    }
  }
}
