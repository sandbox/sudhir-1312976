<?php

/**
 * Implements hook_views_data_alter().
 */

function uc_price_filter_views_data_alter(&$data) {
  if (isset($data['uc_products']['sell_price'])) {
    $data['uc_products']['sell_price']['argument']['handler'] = 'uc_price_filter_handler_argument_gele';    
  }
}

/**
 * Implementation of hook_views_handlers().
 */
function uc_price_filter_views_handlers() {
  return array(
   'info' => array(
      'path' => drupal_get_path('module', 'uc_price_filter') .'/views',
    ),
    'handlers' => array(
      'uc_price_filter_handler_argument_gele' => array(
        'parent' => 'views_handler_argument',
      ),
    )
  );
}
