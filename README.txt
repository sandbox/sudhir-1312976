The module provides the most sought after price filtering capabilities for Ubercart

Installation:
Extract the module in the modules folder and enable it.

Usage:
The module provides a View with Block and Page. The block defines the link with price filters and
the page displays the result

Customization:
Change the price range in the View -> Block -> Custom field. Make sure that the Start and End numbers
used are also added to the link in the field.
